package az.atl.ms_message;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"az.atl.ms_message","az.atl.ms_auth"})
public class MsMessageApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsMessageApplication.class, args);
    }

}
