package az.atl.ms_message.handler;


import az.atl.ms_auth.model.consts.ExceptionMessages;
import az.atl.ms_message.exception.MessageNotFoundException;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.bind.annotation.ExceptionHandler;


import java.time.LocalDateTime;
import java.util.Locale;


@RestControllerAdvice
@RequiredArgsConstructor
@Slf4j
public class MessageExceptionHandler extends ResponseEntityExceptionHandler {

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleAllExceptions(HttpServletRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage("error.internalServerError", null, locale);
        return getResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR, request, message);
    }


    @ExceptionHandler( MessageNotFoundException.class)
    public ResponseEntity<Object> handle(MessageNotFoundException exception, HttpServletRequest request) {
        Locale locale = LocaleContextHolder.getLocale();
        String message = messageSource.getMessage(ExceptionMessages.MESSAGE_NOT_FOUND.getMessage(), new Object[]{exception.getMessage()}, locale);
        return getResponseEntity(HttpStatus.BAD_REQUEST, request, message);
    }

    private ResponseEntity<Object> getResponseEntity(HttpStatus status, HttpServletRequest request, String message) {
        ErrorResponse response = ErrorResponse.builder()
                .message(message)
                .statusCode(status.value())
                .path(request.getRequestURI())
                .timestamp(LocalDateTime.now())
                .build();

        return new ResponseEntity<>(response, status);
    }

}
