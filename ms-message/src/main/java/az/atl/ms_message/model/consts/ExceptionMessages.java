package az.atl.ms_message.model.consts;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ExceptionMessages {
    MESSAGE_NOT_FOUND("message.notFound");

    private final String message;

}
